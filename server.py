import socket
sock = socket.socket()

sock.bind(('', 9090))
sock.listen(1)

conn, addr = sock.accept()
print('connected:', addr)

while True:
    data = conn.recv(1024)
    if not data:
        print("No data")
        break
    elif data == b"Do you understand me?":
        conn.send(b"Yes, I do!")
    elif data == b"Are you robot?":
        conn.send(b"Yes, I am.")
    elif data == b"Are you human?":
        conn.send(b"No.")
    elif data == b"How are you?":
        conn.send(b"I am fine!")

conn.close()

"""import socket
sock = socket.socket()

sock.bind(('', 9090))
sock.listen(1)

conn, addr = sock.accept()
print('connected:', addr)

def listiner(port):
    while True:
        data = conn.recv(1024)
        if not data:
            break
        if data == b"Do you understand me?":
            conn.send(b"Yes, I do!")
        else:
            break

    conn.close()

listiner()"""